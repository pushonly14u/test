

# Set variables common for all OSEv3 hosts
[OSEv3:vars]
###############################################################################
# Common/ Required configuration variables follow                             #
###############################################################################

# THINGS TO FIX
openshift_disable_check=disk_availability,docker_storage
openshift_additional_repos=[{'id': 'centos-okd-ci', 'name': 'centos-okd-ci', 'baseurl' :'http://mirror.centos.org/centos/7/paas/x86_64/openshift-origin311/', 'gpgcheck' :'0', 'enabled' :'1'}]
# END THINGS TO FIX

# install containerized openshift
# containerized=true


# SSH user, this user should allow ssh based auth without requiring a
# password. If using ssh key based auth, then the key should be managed by an
# ssh agent.
ansible_ssh_user=ec2-user

# If ansible_user is not root, ansible_become must be set to true and the
# user must be configured for passwordless sudo
ansible_become=true

# Specify the deployment type. Valid values are origin and openshift-enterprise
openshift_deployment_type=origin
openshift_is_atomic=false

# Specify the generic release of OpenShift to install. This is used mainly just during installation, after which we
# rely on the version running on the first master. Works best for containerized installs where we can usually
# use this to lookup the latest exact version of the container images, which is the tag actually used to configure
# the cluster. For RPM installations we just verify the version detected in your configured repos matches this
# release.
openshift_release="3.11"

# default subdomain to use for exposed routes, you should have wildcard dns
# for *.apps.test.example.com that points at your infra nodes which will run
# your router
openshift_master_default_subdomain=${OKD_SUBDOMAIN}

# set the external master URLs (the load balancer)
openshift_master_public_api_url="https://cluster.{{ openshift_master_default_subdomain }}:8443"
openshift_master_public_console_url="https://cluster.{{ openshift_master_default_subdomain }}:8443/console"

###############################################################################
# Additional configuration variables follow                                   #
###############################################################################

# Debug level for all OpenShift components (Defaults to 2)
debug_level=2

# Specify an exact container image tag to install or configure.
# WARNING: This value will be used for all hosts in containerized environments, even those that have another version installed.
# This could potentially trigger an upgrade and downtime, so be careful with modifying this value after the cluster is set up.
# openshift_image_tag=v3.9

# Manage openshift example imagestreams and templates during install and upgrade
openshift_install_examples=true

# Docker Configuration
# Add additional, insecure, and blocked registries to global docker configuration
# For enterprise deployment types we ensure that registry.access.redhat.com is
# included if you do not include it
#openshift_docker_insecure_registries=registry.example.com
#openshift_docker_blocked_registries=registry.hacker.com

# Skip upgrading Docker during an OpenShift upgrade, leaves the current Docker version alone.
# docker_upgrade=False

# If oreg_url points to a registry requiring authentication, provide the following:
#oreg_auth_user=some_user
#oreg_auth_password='my-pass'
# NOTE: oreg_url must be defined by the user for oreg_auth_* to have any affect.
# oreg_auth_pass should be generated from running docker login.
# To update registry auth credentials, uncomment the following:
#oreg_auth_credentials_replace: True

# If the image for etcd needs to be pulled from anywhere else than registry.access.redhat.com, e.g. in
# a disconnected and containerized installation, use osm_etcd_image to specify the image to use:
#osm_etcd_image=rhel7/etcd

# htpasswd auth
openshift_master_htpasswd_users={'testUser': '$apr1$FdQzuiSc$Z8lmzIBdBU0ztOIlPfmfX1'}
openshift_master_identity_providers=[{"name": "gitlab", "login": "true", "challenge": "true", "kind": "OpenIDIdentityProvider", "client_id": "${GITLAB_APP_ID}", "client_secret": "${GITLAB_APP_SECRET}", "claims": {"id": ["sub"], "preferredUsername": ["nickname"], "name": ["name"], "email": ["email"]}, "urls": {"authorize": "https://gitlab.troweprice.com/oauth/authorize", "token": "https://gitlab.troweprice.com/oauth/token", "userInfo": "https://gitlab.troweprice.com/oauth/userinfo"}}, {'name': 'htpasswd_auth', 'login': 'true', 'challenge': 'true', 'kind': 'HTPasswdPasswordIdentityProvider'}]
openshift_master_openid_ca_file="${GITLAB_CERT_LOCATION}"
# AWS
# openshift_cloudprovider_kind=aws
openshift_clusterid=${OKD_CLUSTER}

# Note: IAM profiles may be used instead of storing API credentials on disk.
#openshift_cloudprovider_aws_access_key=aws_access_key_id
#openshift_cloudprovider_aws_secret_key=aws_secret_access_key

# Enable cockpit
osm_use_cockpit=true
#
# Set cockpit plugins
osm_cockpit_plugins=['cockpit-kubernetes']

# Native high availability (default cluster method)
openshift_master_cluster_method=native
# If no lb group is defined, the installer assumes that a load balancer has
# been preconfigured. For installation the value of
# openshift_master_cluster_hostname must resolve to the load balancer
# or to one or all of the masters defined in the inventory if no load
# balancer is present.
openshift_master_cluster_hostname="cluster.internal.{{ openshift_master_default_subdomain }}"

# If an external load balancer is used public hostname should resolve to
# external load balancer address
openshift_master_cluster_public_hostname="cluster.{{ openshift_master_default_subdomain }}"

# OpenShift Router Options
#
# An OpenShift router will be created during install if there are
# nodes present with labels matching the default router selector,
# "node-role.kubernetes.io/infra=true".
#
# Example:
# [nodes]
# node.example.com openshift_node_group_name="node-config-infra"
#
# Router selector (optional)
# Router will only be created if nodes matching this label are present.
# Default value: 'node-role.kubernetes.io/infra=true'
#openshift_hosted_router_selector='node-role.kubernetes.io/infra=true'
#
#
# Router certificate (optional)
# Provide local certificate paths which will be configured as the
# router's default certificate.
#openshift_hosted_router_certificate={"certfile": "/path/to/router.crt", "keyfile": "/path/to/router.key", "cafile": "/path/to/router-ca.crt"}
#
# Manage the OpenShift Router (optional)
#openshift_hosted_manage_router=true
#

# OpenShift Registry Console Options
# Override the console image prefix:
# origin default is "cockpit/", enterprise default is "openshift3/"
#openshift_cockpit_deployer_prefix=registry.example.com/myrepo/
# origin default is "kubernetes", enterprise default is "registry-console"
#openshift_cockpit_deployer_basename=my-console
# Override image version, defaults to latest for origin, vX.Y product version for enterprise
#openshift_cockpit_deployer_version=1.4.1

openshift_additional_ca="{{ lookup('env', 'CLUSTER_CERT') }}"

# Ansible service broker
ansible_service_broker_install=false

# Openshift Registry Options
#
# An OpenShift registry will be created during install if there are
# nodes present with labels matching the default registry selector,
# "node-role.kubernetes.io/infra=true".
#
# Example:
# [nodes]
# node.example.com openshift_node_group_name="node-config-infra"
#
# Registry selector (optional)
# Registry will only be created if nodes matching this label are present.
# Default value: 'node-role.kubernetes.io/infra=true'
# openshift_hosted_registry_selector='node-role.kubernetes.io/infra=true'
#
# Registry replicas (optional)
# Unless specified, openshift-ansible will calculate the replica count
# based on the number of nodes matching the openshift registry selector.
#openshift_hosted_registry_replicas=2
#
# Validity of the auto-generated certificate in days (optional)
#openshift_hosted_registry_cert_expire_days=730
#
# Manage the OpenShift Registry (optional)
#openshift_hosted_manage_registry=true
# Manage the OpenShift Registry Console (optional)
#openshift_hosted_manage_registry_console=true
#
# AWS S3
# S3 bucket must already exist.
openshift_hosted_registry_storage_kind=object
openshift_hosted_registry_storage_provider=s3
openshift_hosted_registry_storage_s3_encrypt=true
openshift_hosted_registry_storage_s3_kmskeyid=${OKD_REGISTRY_KEY}
# openshift_hosted_registry_storage_s3_accesskey=aws_access_key_id
# openshift_hosted_registry_storage_s3_secretkey=aws_secret_access_key
openshift_hosted_registry_storage_s3_bucket=${OKD_REGISTRY_BUCKET}
openshift_hosted_registry_storage_s3_region=${REGION}
openshift_hosted_registry_storage_s3_chunksize=26214400
openshift_hosted_registry_storage_s3_rootdirectory=/registry
openshift_hosted_registry_pullthrough=true
openshift_hosted_registry_acceptschema2=true
openshift_hosted_registry_enforcequota=true

# Metrics deployment
# See: https://docs.openshift.com/enterprise/latest/install_config/cluster_metrics.html
#
# By default metrics are not automatically deployed, set this to enable them
openshift_metrics_install_metrics=true
#
# Storage Options
# If openshift_metrics_storage_kind is unset then metrics will be stored
# in an EmptyDir volume and will be deleted when the cassandra pod terminates.
# Storage options A & B currently support only one cassandra pod which is
# generally enough for up to 1000 pods. Additional volumes can be created
# manually after the fact and metrics scaled per the docs.
#
# Option C - Dynamic -- If openshift supports dynamic volume provisioning for
# your cloud platform use this.
#openshift_metrics_storage_kind=dynamic
#
# Other Metrics Options -- Common items you may wish to reconfigure, for the complete
# list of options please see roles/openshift_metrics/README.md


# Configure the multi-tenant SDN plugin (default is 'redhat/openshift-ovs-subnet')
os_sdn_network_plugin_name='redhat/openshift-ovs-multitenant'

# Enable service catalog
openshift_enable_service_catalog=true

# Enable template service broker (requires service catalog to be enabled, above)
template_service_broker_install=true

# autoapprove csrs
openshift_master_bootstrap_auto_approve=true
openshift_master_bootstrap_auto_approver_node_selector={"node-role.kubernetes.io/compute":"true", "node-role.kubernetes.io/master":"true", "node-role.kubernetes.io/infra":"true"}
