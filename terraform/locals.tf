locals {
  okd_registry_bucket = "${var.app_name}-${var.aws_account}-${var.aws_region}-okd-${var.short_uniquifier != "" ? var.short_uniquifier : var.environment}-registry"
  okd_backup_bucket   = "${var.app_name}-${var.aws_account}-${var.aws_region}-okd-${var.short_uniquifier != "" ? var.short_uniquifier : var.environment}-backup"
  okd_admin_bucket    = "${var.app_name}-all-${var.aws_account}-${var.aws_region}-okd-${var.short_uniquifier != "" ? var.short_uniquifier : var.environment}-admin"
  uniq_prefix_long    = "${var.app_name}-${var.aws_account}-${var.aws_region}-okd-${var.uniquifier != "" ? var.uniquifier : var.environment}"
  uniq_prefix         = "${var.app_name}-okd-${var.uniquifier != "" ? var.uniquifier : var.environment}"
  uniq_prefix_short   = "${var.app_name}-${var.short_uniquifier != "" ? var.short_uniquifier : var.environment}"
  uniq_suffix         = "okd-${var.uniquifier != "" ? var.uniquifier : var.environment}"
  base_dns            = "${format("${var.base_domain}", "${var.uniquifier != "" ? "${var.environment}.${var.uniquifier}" : var.environment}")}"
}
