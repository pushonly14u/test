module "okd_admin_bucket" {
  source = "./modules/s3_bucket"

  aws_account = "${var.aws_account}"
  aws_region  = "${var.aws_region}"
  bucket_name = "${local.okd_admin_bucket}"
  kms_key_arn = "${data.aws_kms_alias.okd.target_key_id}"
}

data "template_file" "inventory_file" {
  template = "${file("../inventory/inventory.tpl")}"

  vars {
    OKD_CLUSTER          = "${local.uniq_prefix}"
    OKD_SUBDOMAIN        = "${local.base_dns}"
    OKD_REGISTRY_KEY     = "${data.aws_kms_alias.okd.target_key_id}"
    OKD_REGISTRY_BUCKET  = "${module.okd_registry_bucket.bucket}"
    REGION               = "${var.aws_region}"
    GITLAB_CERT_LOCATION = "${var.gitlab_cert_location}"
    GITLAB_APP_ID        = "${var.gitlab_app_id}"
    GITLAB_APP_SECRET    = "${var.gitlab_app_secret}"
  }
}

resource "aws_s3_bucket_object" "inventory_file" {
  key                    = "inventory/inventory.yml"
  bucket                 = "${module.okd_admin_bucket.bucket}"
  content                = "${data.template_file.inventory_file.rendered}"
  server_side_encryption = "aws:kms"
}
