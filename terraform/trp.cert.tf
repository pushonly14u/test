provider "aws" {
  alias   = "dns"
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
  version = "~> 1.1"

  assume_role {
    role_arn     = "${var.assume_role_arn}"
    session_name = "dns"
  }
}

# --- ES provider for SSL certificate management
provider "enterprise-security" {
  token            = "${var.vault-token}"
  github_org       = "okd"
  address          = "https://vault.dev.troweprice.io:8200"
  namespace_domain = "troweprice"
}

resource "enterprise-security_ssl" "certificate" {
  ttl         = "8760h"
  ip_sans     = "127.0.0.1"
  common_name = "*.${local.base_dns}"
}

resource "aws_iam_server_certificate" "service_certificate" {
  name_prefix      = "${local.uniq_prefix}"
  certificate_body = "${enterprise-security_ssl.certificate.certificate}"
  private_key      = "${enterprise-security_ssl.certificate.private_key}"

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "selected" {
  provider     = "aws.dns"
  name         = "awstrp.net."
  private_zone = true
}
