resource "aws_launch_configuration" "master-launch-profile" {
  name_prefix          = "${var.app_name}-openshift-master"
  image_id             = "${data.aws_ami.latest_trp_ami.id}"
  instance_type        = "${var.cluster_instance_type}"
  iam_instance_profile = "${var.ec2_instance_profile}"
  user_data            = "${file("./resources/node.cluster.userdata.sh")}"

  security_groups = [
    "${data.aws_security_group.openshift-master.id}",
    "${data.aws_security_group.openshift-member.id}",
    "${data.aws_security_group.audit-access.id}",
    "${data.aws_security_group.sg_internal_web_elb_8443.id}",
    "${data.aws_security_group.sg_bastion.id}",
  ]

  ebs_block_device {
    volume_size = 80
    device_name = "/dev/xvdh"
    encrypted   = true
  }

  lifecycle {
    ignore_changes        = ["ebs_block_device"]
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "os_master" {
  name                 = "${local.uniq_prefix}-os-master"
  vpc_zone_identifier  = ["${data.aws_subnet_ids.private.ids}"]
  max_size             = "${var.master_max_count}"
  min_size             = "${var.master_min_count}"
  desired_capacity     = "${var.master_desired_count}"
  force_delete         = true
  launch_configuration = "${aws_launch_configuration.master-launch-profile.name}"
  load_balancers       = ["${aws_elb.openshift_internal.name}", "${aws_elb.openshift_client.name}"]

  tags = ["${list(
    map("key", "AutoStart", "value", "on", "propagate_at_launch", true),
    map("key", "ProjectCode", "value", "${var.project_code}", "propagate_at_launch", true),
    map("key", "OwnerEmail", "value", "${var.owner_email}", "propagate_at_launch", true),
    map("key", "CostCenter", "value", "${var.cost_center}", "propagate_at_launch", true),
    map("key", "AppID", "value", "${var.app_id}", "propagate_at_launch", true),
    map("key", "AppName", "value", "${var.app_name}", "propagate_at_launch", true),
    map("key", "Component", "value", "${var.component}", "propagate_at_launch", true),
    map("key", "Terraform", "value", "true", "propagate_at_launch", true),
    map("key", "VulnMgmtAuthCompliance", "value", "true", "propagate_at_launch", true),
    map("key", "AMICompliance", "value", "true", "propagate_at_launch", true),
    map("key", "Schedule", "value", "AlwaysUp", "propagate_at_launch", true),
    map("key", "Name", "value", "${var.app_name}-openshift-master", "propagate_at_launch", true),
    map("key", "scheduler:ebs-snapshot", "value", "false", "propagate_at_launch", true),
    map("key", "okdCluster", "value", "${local.uniq_prefix}", "propagate_at_launch", true),
    map("key", "okdRole", "value", "master", "propagate_at_launch", true),
    map("key", "okdState", "value", "pending", "propagate_at_launch", true),
    map("key", "etcdState", "value", "pending", "propagate_at_launch", true)
    )}"]

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["desired_capacity", "max_size", "min_size"]
  }
}

resource "aws_autoscaling_schedule" "master" {
  scheduled_action_name  = "master_schedule"
  min_size               = 1
  desired_capacity       = -1
  max_size               = -1
  recurrence             = "0 0 * * *"
  autoscaling_group_name = "${aws_autoscaling_group.os_master.name}"
}
