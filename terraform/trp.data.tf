# AMI
data "aws_ami" "latest_trp_ami" {
  owners      = ["self"]
  name_regex  = "^TRP-RHEL-7.6-CIS-encrypted*"
  most_recent = true

  filter {
    name   = "tag:trpimagestatus"
    values = ["approved"]
  }
}

# okd KMS KEY
data "aws_kms_alias" "okd" {
  name = "alias/${var.aws_kms_alias}"
}

# security groups
data "aws_security_group" "sg_bastion" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-bastion"
}

data "aws_security_group" "sg_internal_web_elb" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-internal-web-elb"
}

data "aws_security_group" "sg_internal_web_elb_8443" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-internal-web-elb-8443"
}

data "aws_security_group" "audit-access" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-audit-access"
}

data "aws_security_group" "openshift-master" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-openshift-master"
}

data "aws_security_group" "openshift-member" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-openshift-member"
}

data "aws_security_group" "openshift-compute" {
  vpc_id = "${data.aws_vpc.available.id}"
  name   = "trp-openshift-compute"
}

# subnets
data "aws_vpc" "available" {
  default = false
  state   = "available"

  filter {
    name   = "tag:Name"
    values = ["${var.aws_account}-01-${var.aws_region}"]
  }
}

# Retrieves private subnets
data "aws_subnet_ids" "private" {
  vpc_id = "${data.aws_vpc.available.id}"

  tags {
    Name = "*-priv*"
  }
}

resource "random_shuffle" "single_subnet" {
  input        = ["${data.aws_subnet_ids.private.ids}"]
  result_count = 1
}
