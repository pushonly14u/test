variable "bucket_name" {
  description = "The state bucket name, will also be used for dynamodb table lock"
}

variable "developer_role" {
  description = "The assigned developer role"
}

variable "git_org" {
  description = "the git orginization project where you application lives"
}

variable "git_repo" {
  description = "the name of your git repo"
}

variable "aws_region" {
  description = "the region to create these buckets in"
}

variable "aws_profile" {
  description = "the profile to use to create the buckets"
}

variable "project_code" {
  description = "the project code for this"
}

variable "cost_center" {
  description = "the cost center to bill to"
}

variable "app_id" {
  description = "the TCO app id"
}

variable "app_name" {
  description = "the TCO app name"
}

variable "owner_email" {
  description = "owner contact email"
}

variable "component" {
  description = "the component name"
}

variable "descriptive_name" {
  description = "the descriptive name"
}
