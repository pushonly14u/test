provider "aws" {
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

data "aws_iam_role" "user_role" {
  name = "${var.developer_role}"
}

module "state_bucket" {
  source = "git::ssh://git@github.awstrp.net/infrastructure-engineering/aws-terraform-modules.git//tf_s3_buckets//s3_state_bucket"

  bucket_name               = "${var.bucket_name}"
  aws_profile               = "${var.aws_profile}"
  aws_region                = "${var.aws_region}"
  tag_project_code          = "${var.project_code}"
  tag_cost_center           = "${var.cost_center}"
  tag_application_id        = "${var.app_id}"
  tag_application_name      = "${var.app_name}"
  tag_component             = "${var.component}"
  tag_name                  = "${var.descriptive_name}"
  tag_owner_email           = "${var.owner_email}"
  allowed_aws_accounts_arns = ["${data.aws_iam_role.user_role.arn}"]
}
