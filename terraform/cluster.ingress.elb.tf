# Public ELB
resource "aws_route53_record" "openshift_app" {
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.selected.zone_id}"
  name     = "*.${local.base_dns}"
  type     = "CNAME"
  ttl      = "60"
  records  = ["${aws_elb.openshift_client.dns_name}"]
}

resource "aws_elb" "openshift_client" {
  name         = "${local.uniq_prefix_short}"
  subnets      = ["${data.aws_subnet_ids.private.ids}"]
  internal     = "true"
  idle_timeout = 600

  security_groups = [
    "${data.aws_security_group.sg_bastion.id}",
    "${data.aws_security_group.sg_internal_web_elb.id}",
    "${data.aws_security_group.sg_internal_web_elb_8443.id}",
  ]

  access_logs {
    bucket        = "trp-main-${var.aws_region}-inf-elb-access"
    bucket_prefix = "${var.app_name}-internal-elb"
    interval      = 5
    enabled       = true
  }

  listener = [
    {
      instance_port      = 8443
      instance_protocol  = "ssl"
      lb_port            = 8443
      lb_protocol        = "ssl"
      ssl_certificate_id = "${aws_iam_server_certificate.service_certificate.arn}"
    },
    {
      instance_port      = 443
      instance_protocol  = "ssl"
      lb_port            = 443
      lb_protocol        = "ssl"
      ssl_certificate_id = "${aws_iam_server_certificate.service_certificate.arn}"
    },
  ]

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTPS:8443/healthz"
    interval            = 30
  }

  tags {
    ProjectCode = "${var.project_code}"
    CostCenter  = "${var.cost_center}"
    AppName     = "${var.app_name}"
    AppId       = "${var.app_id}"
    Component   = "${var.git_org}_${var.git_repo}"
    OwnerEmail  = "${var.owner_email}"
    Name        = "${var.app_name}-openshfit-internal"
    Terraform   = "true"
    Schedule    = "AlwaysUp"
  }
}

resource "aws_load_balancer_policy" "okd_proxy_protocol" {
  load_balancer_name = "${aws_elb.openshift_client.name}"
  policy_name        = "okd-ProxyProtocol"
  policy_type_name   = "ProxyProtocolPolicyType"

  policy_attribute = {
    name  = "ProxyProtocol"
    value = "true"
  }
}

# internal cluster ELB
resource "aws_route53_record" "openshift_internal" {
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.selected.zone_id}"
  name     = "cluster.internal.${local.base_dns}"
  type     = "CNAME"
  ttl      = "60"
  records  = ["${aws_elb.openshift_internal.dns_name}"]
}

resource "aws_elb" "openshift_internal" {
  name            = "${local.uniq_prefix_short}-internal"
  subnets         = ["${data.aws_subnet_ids.private.ids}"]
  security_groups = ["${data.aws_security_group.sg_internal_web_elb_8443.id}"]
  internal        = "true"

  access_logs {
    bucket        = "trp-main-${var.aws_region}-inf-elb-access"
    bucket_prefix = "${var.app_name}-internal-elb"
    interval      = 5
    enabled       = true
  }

  listener {
    instance_port     = 8443
    instance_protocol = "tcp"
    lb_port           = 8443
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:8443"
    interval            = 30
  }

  tags {
    ProjectCode = "${var.project_code}"
    CostCenter  = "${var.cost_center}"
    AppName     = "${var.app_name}"
    AppId       = "${var.app_id}"
    Component   = "${var.git_org}_${var.git_repo}"
    OwnerEmail  = "${var.owner_email}"
    Name        = "${var.app_name}-openshfit-internal"
    Terraform   = "true"
    Schedule    = "AlwaysUp"
  }
}
