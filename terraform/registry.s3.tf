module "okd_registry_bucket" {
  source = "./modules/s3_bucket"

  aws_account = "${var.aws_account}"
  aws_region  = "${var.aws_region}"
  bucket_name = "${local.okd_registry_bucket}"
  kms_key_arn = "${data.aws_kms_alias.okd.target_key_id}"
}
