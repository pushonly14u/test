####################################################################################################
# --- Environmet/AWS account specific tfvars file.
# ---    provides an example of an enviroment.tfvars file for an AWS account.
# ---    The variables in this file will need to be set across all accounts deployed.
# ---    NO SPACES

# --- TF_VAR file conventions
# ---   2. The combination of global.tfvars and environmet.tfvars are needed for a successfull deployment
# ---   3. use lower case.
# ---    NO SPACES

####################################################################################################
# --- name space information

# --- aws_account - the trp aws account alias
# --- aws_profile - the aws profile name to use for authorization and authentication.  Found in ~/.aws/credentials
# --- aws_region  - aws region to configure resources.
# --- vpc_id      - the alpha-numeric id of the vpc
aws_account="trp-investments-dev"
aws_profile="trp-investments-dev-okd"
aws_region="us-east-1"

cluster_instance_type="t2.xlarge"
compute_max_count=3
compute_min_count=0
compute_desired_count=0
master_max_count=3
master_min_count=0
master_desired_count=0

assume_role_arn="arn:aws:iam::814303201058:role/trp-dns-awstrpnet-dev"
