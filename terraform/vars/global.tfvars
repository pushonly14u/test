# Global variables that won't change across environments
# NO SPACES
# --- name space information
# --- github_org  = the defined github enterprise organization in github.awstrp.net
# --- github_repo = the github enterprise repository under the github organization
# NO SPACES
git_org="front-office"
git_repo="openshift"

# --- NOTE: application_name needs to be correct (based on the MAR role.)  - this will also be used for the base_name in the
# NO SPACES
# ---       environment.tfvars file.  See that file for more detail.
# NO SPACES
project_code="100792"
cost_center="1020651"
app_name="okd"
app_id="APP10362"
component="infrastructure"
owner_email="Quant_Field_Engineering@troweprice.com"
descriptive_name="okd infrastructure"


# TRP AWS Specific Configuration
aws_kms_alias="okd"
ec2_instance_profile="ec2-okd"
ansible_ec2_instance_profile="ec2-okd-ansible"
services_role_arn="services-okd"
# developer_role="vault-role-okd-deployer"
developer_role="adfs-okddeveloper"

base_domain="os.%s.awstrp.net"
