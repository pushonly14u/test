data "template_file" "userdata" {
  template = "${file("resources/node.admin.userdata.tpl")}"

  vars {
    okd_key                     = "${file("resources/okd-${var.environment}.pub")}"
    REGION                      = "${var.aws_region}"
    ADMIN_BUCKET_NAME           = "${module.okd_admin_bucket.bucket}"
    KMS_KEY                     = "${data.aws_kms_alias.okd.target_key_id}"
    OKD_CLUSTER_ID              = "${local.uniq_prefix}"
    MASTER_ASG                  = "${aws_autoscaling_group.os_master.name}"
    COMPUTE_ASG                 = "${aws_autoscaling_group.os_compute.name}"
    OKD_SUBDOMAIN               = "${local.base_dns}"
    KMS_KEY                     = "${data.aws_kms_alias.okd.target_key_id}"
    ENABLE_SSH_SSM_COMMAND_NAME = "${aws_ssm_document.caf_enable_okd_ssh.name}"  // need to replace caf
  }
}

resource "aws_launch_configuration" "admin-launch-profile" {
  name_prefix          = "${var.app_name}-openshift-admin"
  image_id             = "${data.aws_ami.latest_trp_ami.id}"
  instance_type        = "${var.cluster_instance_type}"
  iam_instance_profile = "${var.ansible_ec2_instance_profile}"
  user_data            = "${data.template_file.userdata.rendered}"

  security_groups = [
    "${data.aws_security_group.sg_bastion.id}",
    "${data.aws_security_group.audit-access.id}",
  ]

  ebs_block_device {
    volume_size = 80
    device_name = "/dev/xvdh"
    encrypted   = true
  }

  lifecycle {
    ignore_changes        = ["ebs_block_device"]
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "os_admin" {
  name                 = "${local.uniq_prefix}-os-admin"
  vpc_zone_identifier  = ["${data.aws_subnet_ids.private.ids}"]
  max_size             = "1"
  min_size             = "1"
  desired_capacity     = "1"
  force_delete         = true
  launch_configuration = "${aws_launch_configuration.admin-launch-profile.name}"

  tags = ["${list(
    map("key", "AutoStart", "value", "on", "propagate_at_launch", true),
    map("key", "ProjectCode", "value", "${var.project_code}", "propagate_at_launch", true),
    map("key", "OwnerEmail", "value", "${var.owner_email}", "propagate_at_launch", true),
    map("key", "CostCenter", "value", "${var.cost_center}", "propagate_at_launch", true),
    map("key", "AppID", "value", "${var.app_id}", "propagate_at_launch", true),
    map("key", "AppName", "value", "${var.app_name}", "propagate_at_launch", true),
    map("key", "Component", "value", "${var.component}", "propagate_at_launch", true),
    map("key", "Terraform", "value", "true", "propagate_at_launch", true),
    map("key", "VulnMgmtAuthCompliance", "value", "true", "propagate_at_launch", true),
    map("key", "AMICompliance", "value", "true", "propagate_at_launch", true),
    map("key", "Schedule", "value", "AlwaysUp", "propagate_at_launch", true),
    map("key", "Name", "value", "${var.app_name}-openshift-admin", "propagate_at_launch", true),
    map("key", "scheduler:ebs-snapshot", "value", "false", "propagate_at_launch", true),
    map("key", "okdCluster", "value", "${local.uniq_prefix}", "propagate_at_launch", true),
    map("key", "okdRole", "value", "admin", "propagate_at_launch", true)
    )}"]

  lifecycle {
    create_before_destroy = true
  }

  depends_on = ["module.okd_admin_bucket", "aws_autoscaling_group.os_master", "aws_autoscaling_group.os_compute"]
}

resource "aws_autoscaling_schedule" "admin" {
  scheduled_action_name  = "admin_schedule"
  min_size               = 1
  desired_capacity       = -1
  max_size               = -1
  recurrence             = "0 0 * * *"
  autoscaling_group_name = "${aws_autoscaling_group.os_admin.name}"
}
