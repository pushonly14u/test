output "OKD_SUBDOMAIN" {
  value = "${local.base_dns}"
}

output "OKD_CLUSTER_ID" {
  value = "${local.uniq_prefix}"
}

output "AWS_REGION" {
  value = "${var.aws_region}"
}

output "OKD_REGISTRY_KEY" {
  value = "${data.aws_kms_alias.okd.target_key_id}"
}

output "OKD_REGISTRY_BUCKET" {
  value = "${module.okd_registry_bucket.bucket}"
}

output "OKD_ADMIN_BUCKET" {
  value = "${module.okd_admin_bucket.bucket}"
}

output "OKD_ADMIN_ASG" {
  value = "${aws_autoscaling_group.os_admin.name}"
}

output "OKD_COMPUTE_ASG" {
  value = "${aws_autoscaling_group.os_compute.name}"
}

output "OKD_MASTER_ASG" {
  value = "${aws_autoscaling_group.os_master.name}"
}

output "POO" {
  value = "${data.template_file.pull_public_key.rendered}"
}

# output "okd-key-arn" {
#   value = "${data.aws_kms_alias.okd.target_key_arn}"
# }


# output internal_dns {
# value = "${aws_route53_record.openshift_internal.name}"
# }


# output cluster_dns {
# value = "cluster.${local.base_dns}"
# }
