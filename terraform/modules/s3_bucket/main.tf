data "aws_iam_policy_document" "admin_bucket_policy" {
  statement {
    sid    = "Enforce HTTPS connections"
    effect = "Deny"

    actions = [
      "s3:*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = ["false"]
    }
  }

  statement {
    sid    = "Deny incorrect encryption header"
    effect = "Deny"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"

      values = [
        "AES256",
        "aws:kms",
      ]
    }
  }

  statement {
    sid    = "Deny plaintext object uploads"
    effect = "Deny"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "Null"
      variable = "s3:x-amz-server-side-encryption"

      values = ["true"]
    }
  }
}

data "aws_iam_policy_document" "admin_bucket_policy_kms" {
  count = "1"

  statement {
    sid    = "Enforce HTTPS connections"
    effect = "Deny"

    actions = [
      "s3:*",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "Bool"
      variable = "aws:SecureTransport"

      values = ["false"]
    }
  }

  statement {
    sid    = "Deny incorrect encryption header"
    effect = "Deny"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"

      values = [
        "aws:kms",
      ]
    }
  }

  statement {
    sid    = "Deny plaintext object uploads"
    effect = "Deny"

    actions = [
      "s3:PutObject",
    ]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    resources = ["arn:aws:s3:::${var.bucket_name}/*"]

    condition {
      test     = "Null"
      variable = "s3:x-amz-server-side-encryption"

      values = ["true"]
    }
  }
}

resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_name}"
  acl    = "private"
  policy = "${data.aws_iam_policy_document.admin_bucket_policy_kms.json}"

  versioning {
    enabled = true
  }

  lifecycle_rule = {
    id      = "TransitionDaysRetainDays"
    enabled = true

    transition {
      days          = "365"
      storage_class = "STANDARD_IA"
    }

    transition {
      days          = "3650"
      storage_class = "GLACIER"
    }

    expiration {
      days = "3710"
    }
  }

  force_destroy = "true"

  tags = "${var.tags}"

  logging {
    target_bucket = "${var.aws_account}-${var.aws_region}-inf-s3bucketlog"
    target_prefix = "${var.bucket_name}/log/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = "${var.kms_key_arn}"
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

output "bucket" {
  value = "${aws_s3_bucket.bucket.bucket}"
}
