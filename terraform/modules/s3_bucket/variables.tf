variable "bucket_name" {}
variable "aws_region" {}
variable "aws_account" {}
variable "kms_key_arn" {}

variable "tags" {
  type        = "list"
  description = "Tags to attach to the bucket"
  default     = []
}
