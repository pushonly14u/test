#!/bin/bash -v
set -o pipefail

# Setup logging function
DATE='date +%Y/%m/%d:%H:%M:%S'
LOG='/home/ec2-user/tf_build'
touch $LOG
chmod 666 $LOG

function log {
    if (( $# == 0 )) ; then
        while IFS= read -r line; do
            echo $($DATE) $line >> $$LOG
        done < /dev/stdin
    else
        echo $($DATE)" $1" >> $LOG
  fi
}

log "Starting ansible configuration"

# Configure ssh key for admins
log "adding okd key"
echo "${okd_key}" >> /home/ec2-user/.ssh/authorized_keys 2>&1 | log

# Configure ssh key for communication with nodes
mkdir -p /home/ec2-user/keys/ 2>&1 | log
ssh-keygen -b 2048 -t rsa -f /home/ec2-user/keys/key -q -P "" 2>&1 | log
mkdir -p /home/ec2-user/.ssh 2>&1 | log
mv /home/ec2-user/keys/key /home/ec2-user/.ssh/id_rsa 2>&1 | log
chown ec2-user:ec2-user /home/ec2-user/.ssh/id_rsa 2>&1 | log

/usr/local/bin/aws --region ${REGION} s3 cp /home/ec2-user/keys/key.pub s3://${ADMIN_BUCKET_NAME}/keys/public --sse aws:kms --sse-kms-key-id ${KMS_KEY} --acl private 2>&1 | log

# Configure environment variables
export OKD_CLUSTER_ID=${OKD_CLUSTER_ID}
echo 'export OKD_CLUSTER_ID=${OKD_CLUSTER_ID}' >> /home/ec2-user/.bashrc

export MASTER_ASG=${MASTER_ASG}
echo 'export MASTER_ASG=${MASTER_ASG}' >> /home/ec2-user/.bashrc

export COMPUTE_ASG=${COMPUTE_ASG}
echo 'export COMPUTE_ASG=${COMPUTE_ASG}' >> /home/ec2-user/.bashrc

export ADMIN_BUCKET_NAME=${ADMIN_BUCKET_NAME}
echo 'export ADMIN_BUCKET_NAME=${ADMIN_BUCKET_NAME}' >> /home/ec2-user/.bashrc

export OKD_SUBDOMAIN=${OKD_SUBDOMAIN}
echo 'export OKD_SUBDOMAIN=${OKD_SUBDOMAIN}' >> /home/ec2-user/.bashrc

export KMS_KEY=${KMS_KEY}
echo 'export KMS_KEY=${KMS_KEY}' >> /home/ec2-user/.bashrc

export ENABLE_SSH_SSM_COMMAND_NAME=${ENABLE_SSH_SSM_COMMAND_NAME}
echo 'export ENABLE_SSH_SSM_COMMAND_NAME=${ENABLE_SSH_SSM_COMMAND_NAME}' >> /home/ec2-user/.bashrc

export CLUSTER_INVENTORY_FILE=/etc/ansible/${OKD_CLUSTER_ID}_inventory.yaml
echo 'export CLUSTER_INVENTORY_FILE=/etc/ansible/${OKD_CLUSTER_ID}_inventory.yaml' >> /home/ec2-user/.bashrc

export REGION=${REGION}
echo 'export REGION=${REGION}' >> /home/ec2-user/.bashrc



chown -R ec2-user:ec2-user /home/ec2-user/keys
chown ec2-user $$LOG

# Install required system packages
yum install -y https://centos7.iuscommunity.org/ius-release.rpm
yum update -y
INSTALL_PKGS="python-lxml python-dns pyOpenSSL python2-cryptography openssl python2-passlib httpd-tools openssh-clients origin-clients iproute patch" \
 && yum install -y --setopt=tsflags=nodocs $INSTALL_PKGS \
 && EPEL_PKGS="ansible python2-boto python2-crypto which python2-pip.noarch python2-scandir python2-packaging azure-cli-2.0.46" \
 && yum install -y epel-release \
 && yum install -y --setopt=tsflags=nodocs $EPEL_PKGS \
 && yum install -y java-1.8.0-openjdk-headless \
 && pip install 'apache-libcloud~=2.2.1' 'SecretStorage<3' 'ansible[azure]' 'boto3' 'passlib' 2>&1 | log

yum install -y git 2>&1 | log

# configure aws cli
mkdir -p /home/ec2-user/.aws
cat << EOF > /home/ec2-user/.aws/config
[default]
region = us-east-1
EOF
chown -R ec2-user:ec2-user /home/ec2-user/.aws

# checkout ansible from openshfit and trp
log 'Checkout openshfit ansible'
/usr/local/bin/aws --region ${REGION} s3 cp s3://${ADMIN_BUCKET_NAME}/inventory/inventory.yml $${CLUSTER_INVENTORY_FILE} --sse aws:kms --sse-kms-key-id ${KMS_KEY} --acl private 2>&1 | log
mkdir -p /home/ec2-user/ansible/ 2>&1 | log
git clone https://gitlab.troweprice.com/okd/trp-ansible.git /home/ec2-user/ansible/trp-ansible 2>&1 | log
log 'Running trp-ansible install'
SCRIPT_USER=ec2-user /home/ec2-user/ansible/trp-ansible/bash/install unattended ec2-user 2>&1 | log
chown -R ec2-user:ec2-user /home/ec2-user/ansible/ 2>&1 | log

# Download cluster cert
mkdir -p /home/ec2-user/certs
touch /home/ec2-user/certs/${OKD_CLUSTER_ID}.pem
export CLUSTER_CERT=/home/ec2-user/certs/${OKD_CLUSTER_ID}.pem
while [[ $(wc -l < /home/ec2-user/certs/${OKD_CLUSTER_ID}.pem) -eq 0 ]]; do
    log waiting for ELB to be assigned
    openssl s_client -showcerts -connect cluster.${OKD_SUBDOMAIN}:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > /home/ec2-user/certs/${OKD_CLUSTER_ID}.pem || true
    sleep 5
done
echo 'export CLUSTER_CERT=/home/ec2-user/certs/${OKD_CLUSTER_ID}.pem' >> /home/ec2-user/.bashrc
chown -R ec2-user:ec2-user /home/ec2-user/certs  2>&1 | log

log "COMPLETE"
