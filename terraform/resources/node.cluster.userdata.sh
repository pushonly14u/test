#!/bin/bash -v
set -o pipefail

DATE='date +%Y/%m/%d:%H:%M:%S'
LOG='/root/tf_build'

touch ${LOG}
chmod 666 ${LOG}

function log {
    if (( $# == 0 )) ; then
        while IFS= read -r line; do
            echo $(${DATE}) ${line} >> $$LOG
        done < /dev/stdin
    else
        echo $(${DATE})" $1" >> ${LOG}
  fi
}

log "enabling ipv4.ip_forwarding"
sed -i 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf 2>&1 | log
sed -i 's/net.ipv4.ip_forward=0/net.ipv4.ip_forward=1/g' /etc/init.d/network 2>&1 | log


log "installing yum deps"
yum install -y yum-utils   device-mapper-persistent-data   lvm2  2>&1 | log

log "add extras repo"
cat << 'EOF' >> /etc/yum.repos.d/extras.repo
[extras]
name=Extras
baseurl=http://centos.s.uw.edu/centos/7/extras/x86_64/
enabled=1
gpgcheck=0
EOF

log "adding epl repo"
rpm -Uvh http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm  2>&1 | log

log "format /dev/xvdh"
umount "/dev/xvdh" 2>&1 | log
printf "o\nn\np\n1\n\n\nw\n" | fdisk "/dev/xvdh" 2>&1 | log
mkfs.ext4 "/dev/xvdh1" 2>&1 | log

log "updating volume group"
pvcreate /dev/xvdh1 -y 2>&1 | log
vgextend uservg /dev/xvdh1 2>&1 | log

log "expanding /var"
lvextend -L40GB /dev/mapper/uservg-lv05 2>&1 | log
resize2fs /dev/mapper/uservg-lv05 2>&1 | log

log "expanding /var/log"
lvextend -L20GB /dev/mapper/uservg-lv07 2>&1 | log
resize2fs /dev/mapper/uservg-lv07 2>&1 | log

chmod a+w /etc/init.d/adjoin-init | log
echo "exit 0" > /etc/init.d/adjoin-init'
chmod a-w /etc/init.d/adjoin-init' | log
pkill adjoin-init | log

echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4" > /etc/hosts
echo "::1         localhost localhost.localdomain localhost6 localhost6.localdomain6" >> /etc/hosts

touch /trp/install_complete

log "COMPLETE"
