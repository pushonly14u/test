data "template_file" "pull_public_key" {
  template = "${file("resources/ssm-commands/pull_public_key.yml")}"

  vars {
    REGION            = "${var.aws_region}"
    ADMIN_BUCKET_NAME = "${module.okd_admin_bucket.bucket}"
    KMS_KEY           = "${data.aws_kms_alias.okd.target_key_id}"
  }
}

resource "aws_ssm_document" "caf_enable_okd_ssh" {  // need to replace caf
  name          = "caf_enable_okd_ssh_${local.uniq_prefix_short}" // need to replace caf
  document_type = "Command"

  document_format = "YAML"

  content = "${data.template_file.pull_public_key.rendered}"

  tags {
    ProjectCode = "${var.project_code}"
    CostCenter  = "${var.cost_center}"
    AppName     = "${var.app_name}"
    AppId       = "${var.app_id}"
    Component   = "${var.git_org}_${var.git_repo}"
    OwnerEmail  = "${var.owner_email}"
    Name        = "${var.app_name}-openshfit-internal"
    Terraform   = "true"
    Schedule    = "AlwaysUp"
  }
}

resource "aws_ssm_document" "deploy_okd_cluster" {
  name          = "caf_deploy_okd_cluster_${local.uniq_prefix_short}"  // need to replace caf
  document_type = "Command"

  document_format = "YAML"

  content = "${file("resources/ssm-commands/cluster_deploy.yml")}"

  tags {
    ProjectCode = "${var.project_code}"
    CostCenter  = "${var.cost_center}"
    AppName     = "${var.app_name}"
    AppId       = "${var.app_id}"
    Component   = "${var.git_org}_${var.git_repo}"
    OwnerEmail  = "${var.owner_email}"
    Name        = "${var.app_name}-openshfit-internal"
    Terraform   = "true"
    Schedule    = "AlwaysUp"
  }
}

resource "aws_ssm_document" "scale_master_okd_cluster" {
  name          = "caf_scale_master_okd_cluster_${local.uniq_prefix_short}"  // need to replace caf
  document_type = "Command"

  document_format = "YAML"

  content = "${file("resources/ssm-commands/master_scaleup.yml")}"

  tags {
    ProjectCode = "${var.project_code}"
    CostCenter  = "${var.cost_center}"
    AppName     = "${var.app_name}"
    AppId       = "${var.app_id}"
    Component   = "${var.git_org}_${var.git_repo}"
    OwnerEmail  = "${var.owner_email}"
    Name        = "${var.app_name}-openshfit-internal"
    Terraform   = "true"
    Schedule    = "AlwaysUp"
  }
}
