variable "vault-token" {
  description = "the vault token used to authenticate to the enterprise security provider"
}

variable "git_org" {
  description = "the git orginization project where you application lives"
}

variable "git_repo" {
  description = "the name of your git repo"
}

variable "aws_region" {
  description = "the region to create these buckets in"
}

variable "aws_account" {
  description = "the aws account"
}

variable "aws_profile" {
  description = "the profile to use to create the buckets"
}

variable "assume_role_arn" {
  description = "the profile to use to create the buckets"
}

variable "environment" {
  description = "the environment this is for"
}

variable "uniquifier" {
  description = "makes everything unique"
}

variable "short_uniquifier" {
  description = "a shorter version which makes everything unique"
}

variable "project_code" {
  description = "the project code for this"
}

variable "cost_center" {
  description = "the cost center to bill to"
}

variable "app_id" {
  description = "the TCO app id"
}

variable "app_name" {
  description = "the TCO app name"
}

variable "owner_email" {
  description = "owner contact email"
}

variable "component" {
  description = "the component name"
}

variable "descriptive_name" {
  description = "the descriptive name"
}

variable "aws_kms_alias" {
  description = "the key alias"
}

variable "ec2_instance_profile" {
  description = "the ec2_instance_profile"
}

variable "ansible_ec2_instance_profile" {
  description = "the ansible_ec2_instance_profile"
}

variable "cluster_instance_type" {
  description = "the cluster instance type"
}

variable "compute_min_count" {
  description = "the ASG min count of the compute instances"
}

variable "compute_max_count" {
  description = "the ASG max count of the compute instances"
}

variable "compute_desired_count" {
  description = "the ASG desired count of the compute instances"
}

variable "master_min_count" {
  description = "the ASG min count of the master instances"
}

variable "master_max_count" {
  description = "the ASG max count of the master instances"
}

variable "master_desired_count" {
  description = "the ASG desired count of the master instances"
}

variable "base_domain" {
  description = "the base domain"
}

variable "gitlab_cert_location" {
  description = "the gitlab cert location on the admin node"
  default     = "/etc/pki/ca-trust/source/anchors/omtcp50675-A5.corp.troweprice.net.intermediate.crt"
}

variable "gitlab_app_id" {
  description = "the assigned gitlab app id"
}

variable "gitlab_app_secret" {
  description = "the assigned gitlab app secret"
}
