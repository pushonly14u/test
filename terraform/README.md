# Install Openshift Infrastructure

## AWS Role creation
1. We are using an automated process to define our credentials.  Links to the policy and instructions on how to get creds can be found [here](https://github.awstrp.net/Security-Engineering/automated-vault-policy/tree/master/force/troweprice/caf)
1. Our credentials are provided through an automated process, documentation can be found [here](https://gitlab.troweprice.com/aws-iam/i-am-walker)

## Terraform organization
1. Main repo `/root/terraform`
   1. All files are separated out by component, for example the ec2 nodes that act as masters get their own file as does compute.  They are both under the 'node' group.
   1. Current setup:
   ```
   data.<component>.tf: used to perform terrafrom data lookups
   ingress.<component>.tf: used to create ingress points to the cluster
   node.<component>.tf: used to store information about the cluster nodes
   main.tf: used to declare locals
   output.tf: defined outputs.  NOTE: these outputs are used in the ansible step.  Remove with caution add with reckless abandon.
   ```
1. State bucket `/root/terraform/bucket`
   1. This stores the terraform needed to create the state bucket
   1. You will not need to interact with this terraform unless there is an issue with the bucket.

## Automated deployment process
1. Terraform is deployed via the gitlab automated pipeline
