provider "aws" {
  profile = "${var.aws_profile}"
  region  = "${var.aws_region}"
  version = "~> 1.1"
}

# --- remote state configuration
terraform {
  backend "s3" {
    acl     = "private"
    encrypt = "true"
  }
}
